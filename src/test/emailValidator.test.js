const { expect } = require("chai");
const { describe, it, beforeEach, afterEach } = require("mocha");
const {
  validate,
  validateAsync,
  validateWithThrow,
  validateWithLog,
} = require("../js/email-validator.js");

describe("validate", () => {
  it("should return true for valid email addresses", () => {
    expect(validate("test@gmail.com")).to.be.true;
    expect(validate("example@outlook.com")).to.be.true;
    expect(validate("user@yandex.ru")).to.be.true;
  });

  it("should return false for invalid email addresses", () => {
    expect(validate("invalid@example.com")).to.be.false;
    expect(validate("notavalidemailaddress")).to.be.false;
    expect(validate("user@testing")).to.be.false;
  });

  it("should return false for empty email address", () => {
    expect(validate("")).to.be.false;
  });
});

describe("validateAsync", () => {
  it("should return true for valid email asynchronously", async () => {
    const result = await validateAsync("test@gmail.com");
    expect(result).to.be.true;
  });

  it("should return false for invalid email asynchronously", async () => {
    const result = await validateAsync("invalid@example.com");
    expect(result).to.be.false;
  });
});

describe("validateWithThrow", () => {
  it("should return true for valid email", () => {
    expect(validateWithThrow("test@gmail.com")).to.be.true;
    expect(validateWithThrow("example@outlook.com")).to.be.true;
    expect(validateWithThrow("user@yandex.ru")).to.be.true;
  });

  it("should throw an error for invalid email", () => {
    expect(() => validateWithThrow("invalid@example.com")).to.throw(
      "Invalid email"
    );
    expect(() => validateWithThrow("user@testing")).to.throw("Invalid email");
  });
});

describe("validateWithLog", () => {
  let consoleOutput = [];
  // eslint-disable-next-line no-console
  const originalLog = console.log;

  beforeEach(() => {
    // Replace console.log with a custom function that captures the log output
    // eslint-disable-next-line no-console
    console.log = (message) => {
      consoleOutput.push(message);
    };
  });

  afterEach(() => {
    // Restore the original console.log behavior
    // eslint-disable-next-line no-console
    console.log = originalLog;
    consoleOutput = [];
  });

  it("should log the result and return true for valid email", () => {
    const result = validateWithLog("test@gmail.com");
    expect(result).to.be.true;
    expect(consoleOutput).to.deep.equal([
      "Email: test@gmail.com, Result: true",
    ]);
  });

  it("should log the result and return false for invalid email", () => {
    const result = validateWithLog("invalid@example.com");
    expect(result).to.be.false;
    expect(consoleOutput).to.deep.equal([
      "Email: invalid@example.com, Result: false",
    ]);
  });
});
