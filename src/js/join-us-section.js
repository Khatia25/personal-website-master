import validate from "./email-validator.js";

const JoinUsSection = (() => {
  function createSection(titleText, subtitleText, buttonText) {
    const appSection = document.createElement("section");
    appSection.classList.add("app-section", "app-section--subscription");

    const sectionTitle = document.createElement("h2");
    sectionTitle.classList.add("app-title");
    sectionTitle.textContent = titleText;

    const appSubtitle = document.createElement("h3");
    appSubtitle.classList.add("app-subtitle");
    appSubtitle.innerHTML = subtitleText;

    const form = document.createElement("form");
    form.classList.add("app-section__form");

    const input = document.createElement("input");
    input.classList.add("app-section__input");
    input.placeholder = "Email";

    const button = document.createElement("button");
    button.classList.add(
      "app-section__button",
      "app-section__button--subscription"
    );
    button.textContent = buttonText;

    function populateEmailFromLocalStorage() {
      const savedEmail = localStorage.getItem("subscriptionEmail");
      if (savedEmail) {
        input.value = savedEmail;
      }
    }

    populateEmailFromLocalStorage();

    function onSubmit(event) {
      event.preventDefault();
      const inputValue = event.target.querySelector("input").value;
      if (validate(inputValue)) {
        localStorage.setItem("subscriptionEmail", inputValue);

        input.style.display = "none";
        input.value = "";
        button.textContent = "Unsubscribe";
        button.style.margin = "auto";
        alert(inputValue);
      }
    }

    function onUnsubscribe() {
      localStorage.removeItem("subscriptionEmail");
      input.style.display = "block";
      button.textContent = "Subscribe";
    }

    button.addEventListener("click", onUnsubscribe);

    form.addEventListener("submit", onSubmit);

    const remove = () => {
      appSection.remove();
    };

    window.addEventListener("load", () => {
      const savedEmail = localStorage.getItem("subscriptionEmail");
      if (savedEmail) {
        input.style.display = "none";
        button.textContent = "Unsubscribe";
        button.style.margin = "auto";
      }
    });

    form.append(input, button);
    appSection.append(sectionTitle, appSubtitle, form);

    return { appSection, remove };
  }

  return {
    createSection,
  };
})();

export default JoinUsSection;
