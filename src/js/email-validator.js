const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

function validate(email) {
  return VALID_EMAIL_ENDINGS.some((ending) => email.endsWith(ending));
}

function validateAsync(email) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(validate(email));
    }, 1000);
  });
}

function validateWithThrow(email) {
  if (validate(email)) {
    return true;
  } else {
    throw new Error("Invalid email");
  }
}

function validateWithLog(email) {
  const result = validate(email);
  // eslint-disable-next-line no-console
  console.log(`Email: ${email}, Result: ${result}`);
  return result;
}

module.exports = {
  validate,
  validateAsync,
  validateWithThrow,
  validateWithLog,
};
