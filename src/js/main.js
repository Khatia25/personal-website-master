import SectionCreator from "./section-creator.js";
import "../styles/style.css";

function initialize() {
  const sectionCreator = new SectionCreator();
  const standardProgram = sectionCreator.create("standard");

  const footer = document.querySelector(".app-footer");
  footer.before(standardProgram.appSection);
}

document.addEventListener("DOMContentLoaded", initialize);

// eslint-disable-next-line no-console
console.log("This line will not be checked for no-console rule");
