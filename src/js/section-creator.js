import JoinUsSection from "./join-us-section.js";

class SectionCreator {
  constructor() {
    if (!SectionCreator.instance) {
      SectionCreator.instance = this;
    }
    return SectionCreator.instance;
  }

  create(type) {
    if (type === "standard") {
      return JoinUsSection.createSection(
        "Join Our Program",
        "Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.",
        "subscribe"
      );
    } else if (type === "advanced") {
      return JoinUsSection.createSection(
        "Join Our Advanced Program",
        "Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.",
        "Subscribe to Advanced Program"
      );
    } else {
      throw new Error("Invalid section type.");
    }
  }
}

export default SectionCreator;
