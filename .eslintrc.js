module.exports = {
  extends: ["eslint:recommended", "plugin:import/errors"],
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: "module",
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  rules: {
    "no-unused-vars": "warn",
    "wrap-iife": "off",
    quotes: ["error", "double", { avoidEscape: true }],
    "no-plusplus": "off",
    "comma-dangle": [
      "error",
      {
        arrays: "always-multiline",
        objects: "ignore",
        imports: "always-multiline",
        exports: "always-multiline",
        functions: "ignore",
      },
    ],
    "no-else-return": "off",
    "class-methods-use-this": "off",
    "no-param-reassign": ["error", { props: false }],
    "import/extensions": ["error", "always", { ignorePackages: true }],
    "no-await-in-loop": "warn",
    "object-curly-newline": ["error", { multiline: true, consistent: true }],
    "linebreak-style": ["error", "windows"],
    "no-console": "error",
  },
};
